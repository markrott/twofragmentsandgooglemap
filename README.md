App consists of one activity and two fragments: 
- top for settings data
- bottom for map

In top fragment we can setup: latitude + longitude coordinates of some location, user photo from device storage. 

Click GO button: we zoom map to selected location. Location presented as marker with inscribed 
user avatar (if avatar is not set up we use common marker).

---

**To develop this application, I used the following technologies:**

- Coroutines
- ViewModel
- LiveData
- MVVM
- Koin
- Glide

---

**Images:**

![Start](/app/src/main/assets/1.jpg)
![Default marker](/app/src/main/assets/2.jpg)
![Custom marker](/app/src/main/assets/3.jpg)
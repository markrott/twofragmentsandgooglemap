package com.example.twofragmentsandgooglemap

import android.app.Application
import com.example.twofragmentsandgooglemap.di.dataModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MyApp)
            modules(dataModule)
        }
    }
}
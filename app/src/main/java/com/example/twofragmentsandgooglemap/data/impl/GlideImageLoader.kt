package com.example.twofragmentsandgooglemap.data.impl

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.twofragmentsandgooglemap.data.contract.ImageLoaderContract

class GlideImageLoader(private val context: Context) : ImageLoaderContract {

    override fun loadAndGetBitmap(path: String): Bitmap {
        return Glide
                .with(context)
                .asBitmap()
                .override(200, 200)
                .load(Uri.parse(path))
                .circleCrop()
                .submit()
                .get()
    }

    override fun loadImage(path: String, view: ImageView) {
        Glide
                .with(context)
                .load(path)
                .override(300, 300)
                .circleCrop()
                .into(view)
    }
}
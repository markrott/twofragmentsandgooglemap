package com.example.twofragmentsandgooglemap.data.contract

import android.graphics.Bitmap
import android.widget.ImageView

interface ImageLoaderContract {

    fun loadAndGetBitmap(path: String) : Bitmap

    fun loadImage(path: String, view: ImageView)
}
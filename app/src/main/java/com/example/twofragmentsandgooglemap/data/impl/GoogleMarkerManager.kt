package com.example.twofragmentsandgooglemap.data.impl

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import com.example.twofragmentsandgooglemap.R
import com.example.twofragmentsandgooglemap.data.contract.ImageLoaderContract
import com.example.twofragmentsandgooglemap.data.contract.MarkerContract
import com.example.twofragmentsandgooglemap.entity.MarkerData
import com.example.twofragmentsandgooglemap.entity.RenderData
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GoogleMarkerManager(
        private val context: Context,
        private val imageLoader: ImageLoaderContract
) : MarkerContract {

    override fun getDefaultMarkerData(position: LatLng): MarkerData =
            MarkerData(position, MarkerOptions().position(position))

    override suspend fun getMarkerData(renderData: RenderData): MarkerData {
        return withContext(Dispatchers.IO) {

            val view = LayoutInflater.from(context).inflate(R.layout.custom_marker, null)
            setupCustomAvatar(view, renderData.pathToAvatar)

            viewMeasure(view)
            val bitmap = createBitmap(view)
            draw(bitmap, view)

            return@withContext MarkerData(
                    renderData.position,
                    getMarkerOptions(renderData.position, bitmap)
            )
        }
    }

    private fun createBitmap(view: View): Bitmap = Bitmap.createBitmap(
            view.measuredWidth, view.measuredHeight,
            Bitmap.Config.ARGB_8888
    )

    private fun getMarkerOptions(position: LatLng, bitmap: Bitmap?): MarkerOptions = MarkerOptions()
            .position(position)
            .icon(BitmapDescriptorFactory.fromBitmap(bitmap))

    private fun draw(bitmap: Bitmap, view: View) {
        val canvas = Canvas(bitmap)
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
        val drawable = view.background
        drawable?.draw(canvas)
        view.draw(canvas)
    }

    private fun viewMeasure(view: View) {
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        view.layout(0, 0, view.measuredWidth, view.measuredHeight)
    }

    private fun setupCustomAvatar(view: View, pathToAvatar: String) {
        val avatar = view.findViewById<AppCompatImageView>(R.id.iv_avatar)
        val bitmap = imageLoader.loadAndGetBitmap(pathToAvatar)
        avatar.setImageBitmap(bitmap)
    }
}
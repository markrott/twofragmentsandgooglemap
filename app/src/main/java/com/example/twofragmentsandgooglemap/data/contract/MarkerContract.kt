package com.example.twofragmentsandgooglemap.data.contract

import com.example.twofragmentsandgooglemap.entity.MarkerData
import com.example.twofragmentsandgooglemap.entity.RenderData
import com.google.android.gms.maps.model.LatLng

interface MarkerContract {

    fun getDefaultMarkerData(position: LatLng): MarkerData

    suspend fun getMarkerData(renderData: RenderData): MarkerData
}
package com.example.twofragmentsandgooglemap.di

import com.example.twofragmentsandgooglemap.data.impl.GlideImageLoader
import com.example.twofragmentsandgooglemap.data.contract.ImageLoaderContract
import com.example.twofragmentsandgooglemap.data.impl.GoogleMarkerManager
import com.example.twofragmentsandgooglemap.data.contract.MarkerContract
import com.example.twofragmentsandgooglemap.ui.viewmodels.SharedViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val dataModule = module {
    single<ImageLoaderContract> { GlideImageLoader(androidApplication()) }
    single<MarkerContract> { GoogleMarkerManager(androidApplication(), get()) }
    viewModel { SharedViewModel(get()) }
}
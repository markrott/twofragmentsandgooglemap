package com.example.twofragmentsandgooglemap.entity

import com.google.android.gms.maps.model.LatLng

data class RenderData(
        val position: LatLng = LatLng(0.0, 0.0),
        val pathToAvatar: String = ""
)
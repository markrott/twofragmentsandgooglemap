package com.example.twofragmentsandgooglemap.entity

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MarkerData(val position: LatLng, val markerOptions: MarkerOptions)
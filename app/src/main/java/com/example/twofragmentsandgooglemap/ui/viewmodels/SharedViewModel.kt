package com.example.twofragmentsandgooglemap.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.twofragmentsandgooglemap.data.contract.MarkerContract
import com.example.twofragmentsandgooglemap.entity.MarkerData
import com.example.twofragmentsandgooglemap.entity.RenderData
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.launch

class SharedViewModel(private val markerManager: MarkerContract) : ViewModel() {

    private var renderData: RenderData = RenderData()
    private val _markerDataMLD = MutableLiveData<MarkerData>()
    val markerDataLD: LiveData<MarkerData> get() = _markerDataMLD

    fun setupPosition(newPosition: LatLng) {
        renderData = renderData.copy(position = newPosition)
        prepareGoogleMarkerData()
    }

    fun setupAvatar(pathToImage: String) {
        renderData = renderData.copy(pathToAvatar = pathToImage)
    }

    private fun prepareGoogleMarkerData() {
        if (renderData.pathToAvatar.isBlank()) {
            defaultMarkerOptions()
        } else {
            customMarkerOptions()
        }
    }

    private fun defaultMarkerOptions() {
        _markerDataMLD.value = markerManager.getDefaultMarkerData(renderData.position)
    }

    private fun customMarkerOptions() {
        viewModelScope.launch {
            try {
                val data = markerManager.getMarkerData(renderData)
                _markerDataMLD.value = data
            } catch (e: Exception) {
                defaultMarkerOptions()
            }
        }
    }
}
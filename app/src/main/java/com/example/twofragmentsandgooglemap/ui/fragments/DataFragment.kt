package com.example.twofragmentsandgooglemap.ui.fragments

import android.Manifest
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import com.example.twofragmentsandgooglemap.R
import com.example.twofragmentsandgooglemap.data.contract.ImageLoaderContract
import com.example.twofragmentsandgooglemap.hideKeyboard
import com.example.twofragmentsandgooglemap.showToast
import com.example.twofragmentsandgooglemap.ui.viewmodels.SharedViewModel
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.frg_data.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class DataFragment : Fragment() {

    private val imageLoader: ImageLoaderContract by inject()
    private val viewModel by sharedViewModel<SharedViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.frg_data, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_set_position.setOnClickListener {

            val rawLat = edt_lat.text.toString().trim()
            val rawLng = edt_lng.text.toString().trim()

            if (rawLat.isNotEmpty() && rawLng.isNotEmpty()) {
                hideKeyboard()
                val lat = rawLat.toDouble()
                val lng = rawLng.toDouble()
                viewModel.setupPosition(LatLng(lat, lng))
            } else requireContext().showToast(getString(R.string.msg_fill_in_all_fields))
        }

        btn_set_photo.setOnClickListener {
            askPickImagePermission.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
    }

    private val askPickImagePermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { hasPermission ->
            if (hasPermission) {
                pickImages.launch("image/*")
            }
        }

    private val pickImages =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
            uri?.let { path ->
                imageLoader.loadImage(path.toString(), iv_avatar)
                viewModel.setupAvatar(path.toString())
            }
        }
}